use std::process::ExitCode;

use anyhow::Result;
use clap::{command, Parser, ValueEnum};
use windows::core::HSTRING;
use windows::Foundation::IAsyncOperation;
use windows::Security::Credentials::{
    KeyCredential, KeyCredentialCreationOption, KeyCredentialManager,
    KeyCredentialStatus,
};

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum Cmd {
    Create,
    Open,
    Delete,
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct App {
    #[arg(value_enum)]
    cmd: Cmd,

    #[arg()]
    account_id: String,
}

fn main() -> ExitCode {
    let app: App = App::parse();
    let account_id: HSTRING = HSTRING::from(app.account_id);

    let is_supported: bool = match is_supported() {
        Err(error) => {
            println!("Error: {}", error);
            return ExitCode::FAILURE;
        }
        Ok(is_supported) => is_supported,
    };
    println!("Is supported: {}", is_supported);

    match app.cmd {
        Cmd::Create => {
            let cred: KeyCredential = match create(
                &account_id,
                KeyCredentialCreationOption::FailIfExists,
            ) {
                Err(error) => {
                    println!("Error: {}", error);
                    return ExitCode::FAILURE;
                }
                Ok(r) => r,
            };

            let cred_name = match cred.Name() {
                Err(error) => {
                    println!("Error: {}", error);
                    return ExitCode::FAILURE;
                }
                Ok(name) => name.to_string(),
            };

            println!("Credential name: {}", cred_name);
        }
        Cmd::Open => {
            let cred: KeyCredential = match open(&account_id) {
                Err(error) => {
                    println!("Error: {}", error);
                    return ExitCode::FAILURE;
                }
                Ok(r) => r,
            };

            let cred_name = match cred.Name() {
                Err(error) => {
                    println!("Error: {}", error);
                    return ExitCode::FAILURE;
                }
                Ok(name) => name.to_string(),
            };

            println!("Credential name: {}", cred_name);
        }
        Cmd::Delete => {
            if let Err(error) = delete(&account_id) {
                println!("Error: {}", error);
                return ExitCode::FAILURE;
            };
        }
    }

    return ExitCode::SUCCESS;
}

fn is_supported() -> Result<bool> {
    let promise: IAsyncOperation<bool> =
        KeyCredentialManager::IsSupportedAsync()?;
    let supported = promise.get()?;
    Ok(supported)
}

fn create(
    name: &HSTRING,
    option: KeyCredentialCreationOption,
) -> Result<KeyCredential> {
    let promise = KeyCredentialManager::RequestCreateAsync(name, option)?;
    let retrieval = promise.get()?;
    let status = retrieval.Status()?;
    if status != KeyCredentialStatus::Success {
        return Err(anyhow::anyhow!(
            "KeyCredentialManager::RequestCreateAsync() failed with status {:?}",
            get_key_credential_status_name(status)
        ));
    }
    let cred: KeyCredential = retrieval.Credential()?;
    Ok(cred)
}

fn open(name: &HSTRING) -> Result<KeyCredential> {
    let promise = KeyCredentialManager::OpenAsync(name)?;
    let retrieval = promise.get()?;
    let status = retrieval.Status()?;
    if status != KeyCredentialStatus::Success {
        return Err(anyhow::anyhow!(
            "KeyCredentialManager::OpenAsync() failed with status {:?}",
            get_key_credential_status_name(status)
        ));
    }
    let cred: KeyCredential = retrieval.Credential()?;
    Ok(cred)
}

fn get_key_credential_status_name(status: KeyCredentialStatus) -> &'static str {
    match status {
        KeyCredentialStatus::Success => "Success",
        KeyCredentialStatus::UnknownError => "UnknownError",
        KeyCredentialStatus::NotFound => "NotFound",
        KeyCredentialStatus::UserCanceled => "UserCanceled",
        KeyCredentialStatus::UserPrefersPassword => "UserPrefersPassword",
        KeyCredentialStatus::CredentialAlreadyExists => {
            "CredentialAlreadyExists"
        }
        KeyCredentialStatus::SecurityDeviceLocked => "SecurityDeviceLocked",
        _ => "(unknown)",
    }
}

fn delete(name: &HSTRING) -> Result<()> {
    let promise = KeyCredentialManager::DeleteAsync(name)?;
    promise.get()?;
    Ok(())
}
