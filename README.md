This repository contains a sample Rust program that uses [Windows Hello] to
authenticate users.

[Windows Hello]: https://learn.microsoft.com/en-us/windows/uwp/security/microsoft-passport
